/*
 * i2c.h
 *
 *  Created on: 05-Jun-2021
 *      Author: Acer
 */

#ifndef I2C_H_
#define I2C_H_
#include "ph_Status.h"
#include "phhalEeprom.h"
#include "phCfg_EE.h"
#include "ph_Status.h"
#include "phhalTimer.h"

#define BUZZER_GPIO			1
typedef struct {
	uint8_t date;
	uint8_t month;
	uint16_t year;
} DATE_STRUCT;

typedef union {
	DATE_STRUCT date_info ;
	uint8_t date_info_array[sizeof(DATE_STRUCT)+1];
}DATE_UNION;

phhalTimer_Timers_t *timer_buzzer_timeout;
enum {negative = 0, positive};
enum {inactive = 0, active};

phStatus_t save_date(DATE_STRUCT *validity_date);
phStatus_t get_date(DATE_STRUCT *validity_date);
phStatus_t check_validity(DATE_STRUCT *current);
phStatus_t init_buzzer_gpio(void);
void play_buzzer(uint8_t no_of_times, uint8_t delay_ms);
void timer_buzzer_callback(void);
unsigned char get_checksum(unsigned char *p, unsigned bytes);


#endif /* I2C_H_ */
