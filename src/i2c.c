/*
 * i2c.c
 *
 *  Created on: 05-Jun-2021
 *      Author: Acer
 */

#include "time.h"
#include "i2c.h"
#include "phhalEeprom.h"
#include "phCfg_EE.h"
#include "ph_Status.h"
#include "phhalGpio.h"
#include "phhalTimer.h"
#include "stdint.h"
#include "stdio.h"
#include "phOsal.h"
#include "phExNFCCcid_LED.h"
#include "phUser.h"
#include "phExNFCCcid.h"

static uint8_t buzzer_status = 0;
extern uint8_t ccid_comms_block_flag;

/**
 * @brief Save user given validity date in internal eep.
 * @param User given date.
 * @retval Status and Play buzzer
 */
phStatus_t save_date(DATE_STRUCT *validity_date){
	unsigned char checksum;
	DATE_UNION date_local_union;
	phStatus_t status = PH_ERR_SUCCESS;
	date_local_union.date_info = *validity_date;
	uint16_t date_array_size = sizeof(date_local_union.date_info_array);

		checksum = get_checksum(date_local_union.date_info_array, (date_array_size - 1));
		date_local_union.date_info_array[date_array_size - 1] = checksum;

	status = phhalEeprom_WriteBuffer(date_local_union.date_info_array,gpkphCfg_EE_HLBlock_ValidityStructure->Validity,date_array_size);

	if (status == PH_ERR_SUCCESS){
		play_buzzer(1,250);
	}
	else{
		play_buzzer(3,250);
	}

		return status;
}

/**
 * @brief Get validity date stored in internal eep.
*/
phStatus_t get_date(DATE_STRUCT *validity_date){
	DATE_UNION date_local_union;
	unsigned char eeprom_checksum = 0, calculated_checksum = 0;
	uint8_t checksum_error = false;
	uint16_t date_array_size = 0;
	phStatus_t status = PH_ERR_SUCCESS;

	date_array_size = sizeof(date_local_union.date_info_array);

	phUser_MemCpy(date_local_union.date_info_array, gpkphCfg_EE_HLBlock_ValidityStructure->Validity,date_array_size);

	calculated_checksum = get_checksum(date_local_union.date_info_array,date_array_size - 1);
	eeprom_checksum = date_local_union.date_info_array[date_array_size - 1];

	if (calculated_checksum != eeprom_checksum) {
		checksum_error = true;
	} else {

	}

	if (checksum_error == true) {
		play_buzzer(3,250);
		checksum_error = false;
	}

	*validity_date = date_local_union.date_info;
	return status;
}

/**
 * @brief Save user given validity date in internal eep.
 * @param Provide current date.
 * @retval Play buzzer for success/error condition.
*/
phStatus_t check_validity (DATE_STRUCT *current){

	DATE_STRUCT validity_date;
	get_date(&validity_date);
	struct tm t;
	time_t current_epoch = 0;
	time_t validity_date_epoch = 0;
	uint8_t response = 0;

	t.tm_year = current->year- 1900;  // Year - 1900
	t.tm_mon = current->month - 1;     // Month, where 0 = jan
	t.tm_mday = current->date;         // Day of the month
	t.tm_hour = 23;
	t.tm_min = 59;
	t.tm_sec = 59;
	t.tm_isdst = -1;        // Is DST on? 1 = yes, 0 = no, -1 = unknown
	current_epoch = mktime(&t);

	t.tm_year = validity_date.year - 1900;  // Year - 1900
	t.tm_mon = validity_date.month - 1;     // Month, where 0 = jan
	t.tm_mday = validity_date.date;         // Day of the month
	t.tm_hour = 23;
	t.tm_min = 59;
	t.tm_sec = 59;
	t.tm_isdst = -1;        // Is DST on? 1 = yes, 0 = no, -1 = unknown
	validity_date_epoch = mktime(&t);

	if (current_epoch >validity_date_epoch) {
	///<	Validity expired
			play_buzzer(3, 250);
			response = negative;
			ccid_comms_block_flag = true;
	}
	else {
	///<	Validity valid
			play_buzzer(1, 250);
			response = positive;
			ccid_comms_block_flag = false;
	}
	return response;
}

/**
 * @brief Set buzzer timer.
*/
void timer_buzzer_callback(void) {
	if(buzzer_status == true) {
		PH_HAL_GPIO_SETGPIOVAL(BUZZER_GPIO,inactive);
		buzzer_status = false;
	}
	else{
		PH_HAL_GPIO_SETGPIOVAL(BUZZER_GPIO,active);
		buzzer_status = true;
	}
}

/**
 * @brief Play buzzer.
 * @param No of times buzzer to be played and time delay in ms
*/
void play_buzzer(uint8_t no_of_times, uint8_t delay_ms) {
	uint8_t i;
	buzzer_status = 0;
	for(i = 0; i<no_of_times; i++) {
		phhalTimer_Start(timer_buzzer_timeout,E_TIMER_FREE_RUNNING);
		phUser_Wait(delay_ms * 1000);
		phhalTimer_Stop(timer_buzzer_timeout);
		phUser_Wait(delay_ms * 1000);
	}
	PH_HAL_GPIO_SETGPIOVAL(BUZZER_GPIO,inactive);
}

/**
 * @brief Initialize buzzer GPIO.
 * @retval Status of initialisation.
*/
phStatus_t init_buzzer_gpio(void) {
	phStatus_t tim_status;
	phhalPcr_ConfigOutput(BUZZER_GPIO, true, false);
	PH_HAL_GPIO_SETGPIOVAL(BUZZER_GPIO,inactive);
///< TIMER For Buzzer
	tim_status = phhalTimer_RequestTimer(E_TUNIT_MICRO_SECS, &timer_buzzer_timeout);
	if(tim_status != PH_ERR_SUCCESS) {
		phhalTimer_DeInit();
	}
	phhalTimer_Configure(timer_buzzer_timeout,183,timer_buzzer_callback);
	return tim_status;
}

unsigned char get_checksum(unsigned char *p, unsigned bytes) {

	unsigned i;
	unsigned char checksum = 0;

	for (i = 0; i < bytes; i++)
		checksum += p[i];

	return checksum;
}

